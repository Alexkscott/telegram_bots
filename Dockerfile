FROM python:3.7

COPY requirements.txt /app/
COPY get_lyrics.py /app/
COPY get_weather.py /app/
COPY slash_bot.py /app/
WORKDIR /app

RUN pip install -r requirements.txt
