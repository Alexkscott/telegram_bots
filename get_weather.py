import json
import requests


def get_weather(api_key, location):
    base_url = "http://api.openweathermap.org/data/2.5/weather?"
    complete_url = base_url + "appid=" + api_key + "&q=" + location + "&units=metric"
    try:
        resp = requests.get(complete_url).json()
        print(resp)
        if resp["cod"] == 200:
            data = resp["main"]
            return data["temp"]
        else:
            return "Sorry something went wrong there: {}".format(resp["cod"])
    except Exception as excinfo:
        print(str(excinfo))
        return str(excinfo)