from PyLyrics import PyLyrics


def get_lyrics(artist, song):
    print(artist)
    formatted_song = song.lstrip()
    print(formatted_song)
    try:
        return PyLyrics.getLyrics(artist, formatted_song)
    except Exception as excinfo:
        return str(excinfo)