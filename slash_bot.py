import sys

from telegram.ext import Updater, CommandHandler,  MessageHandler, Filters

from get_lyrics import get_lyrics
from get_weather import get_weather


class SlashBot:
    def __init__(self, telegram_token, weather_token):
        self.telegram_token = telegram_token
        self.weather_token = str(weather_token)
        self.command_list = []

    def send_message(self, bot, chat_id, message):
        bot.send_message(chat_id=chat_id, text=message)

    def start(self, bot, update):
        self.send_message(bot, update.message.chat_id, "I'm a bot, please talk to me")

    def echo(self, bot, update):
        self.send_message(bot, update.message.chat_id,
                          update.message.text)

    def caps(self, bot, update, args):
        if "-help" in args:
            message = "Enter words to capitalise"
        else:
            message = ' '.join(args).upper()
        self.send_message(bot, update.message.chat_id, message)

    def weather(self, bot, update, args):
        if args:
            if "-help" in args:
                message = "<Location>"
            else:
                message = get_weather(self.weather_token, args[0])
        else:
            message = "Something went wrong"
        self.send_message(bot, update.message.chat_id, message)

    def sing(self, bot, update, args):
        if "-help" in args:
            message = "<Artist's name>, <Song name>"
        else:
            text = ' '.join(args)
            artist, song = text.split(',')
            lyrics = get_lyrics(artist, song)
            message = lyrics
        self.send_message(bot, update.message.chat_id, message)
    
    def commands(self, dispatcher):
        self.command_list = []
        for _, item in dispatcher.handlers.items():
            for a in item:
                try:
                    self.command_list.append("/{}".format(a.command[0]))
                except AttributeError:
                    continue

    def get_commands(self, bot, update):
        self.send_message(bot, update.message.chat_id, " ".join(self.command_list))

    def unknown(self, bot, update):
        self.send_message(bot, update.message.chat_id, "Sorry, I didn't understand that command")

    def run(self):
        updater = Updater(token=self.telegram_token)
        dispatcher = updater.dispatcher
        start_handler = CommandHandler('start', self.start)
        dispatcher.add_handler(start_handler)
        echo_handler = MessageHandler(Filters.text, self.echo)
        dispatcher.add_handler(echo_handler)
        caps_handler = CommandHandler('caps', self.caps, pass_args=True)
        dispatcher.add_handler(caps_handler)
        weather_handler = CommandHandler('weather', self.weather, pass_args=True)
        dispatcher.add_handler(weather_handler)
        sing_handler = CommandHandler('sing', self.sing, pass_args=True)
        dispatcher.add_handler(sing_handler)
        get_commands_handler = CommandHandler('commands', self.get_commands)
        dispatcher.add_handler(get_commands_handler)
        unknown_handler = MessageHandler(Filters.command, self.unknown)
        dispatcher.add_handler(unknown_handler)
        self.commands(dispatcher)
        updater.start_polling()


if __name__ == '__main__':
    telegram_token = sys.argv[1]
    weather_token = sys.argv[2]
    bot = SlashBot(telegram_token, weather_token)
    bot.run()
