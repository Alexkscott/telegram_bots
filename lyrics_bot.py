import time

import telegram

import config


def run():
    bot = telegram.Bot(token=config.token)
    chat_id = None
    for update in bot.get_updates():
        if update.message.chat.type == 'group':
            chat_id = update.message.id
    with open("lyrics.txt") as f:
        for line in f:
            bot.send_message(chat_id=chat_id, text=line)
            time.sleep(10)


if __name__ == '__main__':
    run()